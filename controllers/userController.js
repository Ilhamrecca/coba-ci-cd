const { user } = require("../models/mongodb"); // import user models
const passport = require("passport"); // import passport
const jwt = require("jsonwebtoken"); // import jsonwebtoken
const bcrypt = require("bcrypt"); // Import bcrypt
class UserController {
	// if user signup
	async signup(user, req, res) {
		// get the req.user from passport authentication
		const body = {
			email: req.body.email,
		};

		// create jwt token from body variable
		const token = jwt.sign(
			{
				user: body,
			},
			"secret_password"
		);

		// success to create token
		res.status(200).json({
			message: "Signup success!",
			token: token,
		});
	}

	// // if user login
	async login(user, req, res) {
		// get the req.user from passport authentication
		const body = {
			email: user.email,
		};

		// create jwt token from body variable
		const token = jwt.sign(
			{
				user: body,
			},
			"secret_password"
		);

		// success to create token
		res.status(200).json({
			message: "Login success!",
			token: token,
		});
	}

	async insertPP(token, req, res
		) {
		// get the req.user from passport authentication

		// create jwt token from body variable
		user.updateOne(
			{
				email: token.email,
			},
			{
				$set: {
					profilePic: req.file === undefined ? "" : req.file.filename,
				},
			}
		).then((result) => {
			res.status(200).json({
				message: "Uploaded Successfuly",
				result: "public/img" + req.file.filename,
			});
		});
		// success to create token
	}

	async changeName(token, req, res) {
		// get the req.user from passport authentication

		// create jwt token from body variable
		user.updateOne(
			{
				email: token.email,
			},
			{
				$set: {
					fullName: req.body.fullName,
				},
			}
		).then((result) => {
			res.status(200).json({
				message: "Name Changed Successfuly",
				result: result,
			});
		});
		// success to create token
	}

	async changePass(token, req, res) {
		// get the req.user from passport authentication

		// create jwt token from body variable
		user.findOne({ email: token.email })
			.then(async (result) => {
				return await bcrypt.compare(
					req.body.oldPassword,
					result.password
				);
			})
			.then((result) => {
				if (result) {
					user.updateOne(
						{
							email: token.email,
						},
						{
							$set: {
								password: bcrypt.hashSync(
									req.body.newPassword,
									10
								),
							},
						}
					).then((result) => {
						res.status(200).json({
							message: "password Changed Successfuly",
							result: result,
						});
					});
				} else {
					throw new Error("password is wrong");
				}
			})
			.catch(() => {
				res.status(400).json({
					message: "Old Password is wrong",
				});
			}); // get the req.user from passport authentication
	}

	async getPP(req, res) {
		user.findOne({ email: req.body.email }).then((result) => {
			res.status(200).json({
				message: "Uploaded Successfuly",
				result: result.profilePic,
			});
		});
	}
}

module.exports = new UserController(); // export UserController
