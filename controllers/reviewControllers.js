const { user, movies } = require("../models/mongodb"); // import user models
const passport = require("passport"); // import passport
const jwt = require("jsonwebtoken"); // import jsonwebtoken
const bcrypt = require("bcrypt"); // Import bcrypt

class reviewController {
	// if user signup
	async addReview(token, req, res, next) {
		const review = {
			id: req.params.id,
			review: req.body.review,
			rating: req.body.rating,
		};

		user.findOne({ review: { $elemMatch: { id: req.params.id } } })
			.then((result) => {
				if (result !== null)
					throw Error("you have reviewed this movie before");
			})
			.catch((err) => {
				res.status(400).json({
					message: "failed to add review",
				});
			});

		user.updateOne({ email: token.email }, { $push: { review } })
			.then((result) => {
				if (result.ok === 0 || result.nModified === 0) {
					throw new Error("something went wrong");
				} else {
					res.status(200).json({
						message: "review added Successfuly",
					});
				}
			})
			.catch((err) => {
				res.status(400).json({
					message: "failed to add review",
				});
			});

		movies.findOne({ id: req.params.id }).then((result) => {
			let rating =
				(parseInt(req.body.rating) + parseInt(result.averageRating)) /
				2;

			movies.updateOne(
				{ id: req.params.id },
				{ $set: { averageRating: rating } }
			);
		});
	}
}

module.exports = new reviewController(); // export UserController
