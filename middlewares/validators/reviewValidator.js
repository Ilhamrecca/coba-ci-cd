const {
	check,
	validationResult,
	matchedData,
	sanitize,
} = require("express-validator"); //form validation & sanitize form params
const { user, movies } = require("../../models/mongodb"); // Import user model

module.exports = {
	addReview: [
		check("rating", "rating must be between 1-10")
			.isNumeric()
			.custom((value) => {
				if (value < 0 || value > 10) {
					return false;
				}
				return true;
			}),
		check("review", "your character must fit around 255")
			.isString()
			.isLength({ max: 255, min: 5 }),
		check("id", "Movie not exist").custom((value) => {
			return movies.findOne({ id: value }).then((result) => {
				if (result === undefined) {
					return false;
				}
				return true;
			});
		}),
		// check("id", "you have reviewed this movie before").isNumeric().custom((value) => {
		//
		// }),

		(req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(422).json({
					errors: errors.mapped(),
				});
			}
			next();
		},
	],
};
