const express = require("express"); // import express
const router = express.Router(); // import router
const passport = require("passport"); // import passport
const auth = require("../middlewares/auth"); // import passport auth strategy
const reviewController = require("../controllers/reviewControllers"); // import userController
const reviewValidator = require("../middlewares/validators/reviewValidator"); // import userValidator

// if user go to localhost:3000/signup
router.post("/addReview/:id", [
	reviewValidator.addReview,
	function (req, res, next) {
		passport.authenticate(
			"checkLogin",
			{
				session: false,
			},
			function (err, user, info) {
				if (err) {
					return next(err);
				}
				if (!user) {
					res.status(401).json({
						status: "Error",
						message: info.message,
					});
					return;
				}

				reviewController.addReview(user, req, res, next);
			}
		)(req, res, next);
	},
]);

router.use((req, res, next) => {
	const err = new Error("Page Not Founf");
	err.status = 404;
	next(err);
});

router.use((err, req, res, next) => {
	res.status(err.status || 500);
	res.send({
		error: {
			status: err.status || 500,
			message: err.message,
		},
	});
});
module.exports = router; // export router
