const mongoose = require("mongoose"); // Import mongoose
const mongoose_delete = require("mongoose-delete"); // Import mongoose-delete to make soft delete

// Make barang model
const moviesSchema = new mongoose.Schema(
	{
		// Define column that we will used
		id: {
			type: String,
			required: true,
		},
		title: {
			type: String,
			required: true,
		},
		year: {
			type: Number,
			default: null,
			required: false,
		},
		genres: {
			type: Array,
			default: null,
			required: false,
		},
		releaseDate: {
			type: Date,
			default: null,
			required: false,
		},
		averageRating: {
			type: Number,
			default: 0,
			required: false,
			// set: setAverageRating,
		},
		storyline: {
			type: String,
			default: null,
			required: false,
		},
		actors: {
			type: Array,
			default: null,
			required: false,
		},
		posterurl: {
			type: String,
			default: null,
			required: false,
		},
	},
	{
		// enable timestamps
		timestamps: {
			createdAt: "created_at",
			updatedAt: "updated_at",
		},
		versionKey: false,
	}
);
// function setAverageRating(rating) {
// 	return (rating + this.averageRating) / 2;
// }

moviesSchema.plugin(mongoose_delete, { overrideMethods: "all" }); // enable soft delete

module.exports = movies = mongoose.model("movies", moviesSchema, "movies"); // export movies model
